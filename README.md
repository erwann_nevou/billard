# Billard

Jeu de billard simple.

https://erwann_nevou.gitlab.io/billard/

Généré avec yeoman [webpack-ts](https://www.npmjs.com/package/generator-webpack-ts)

```bash
# Fetch dependencies
npm install

# Start webpack-dev-server and watch for changes
npm start
```

## Start command-line http server

Using [http-server](https://www.npmjs.com/package/http-server), a simple zero-configuration command-line http server

```properties
npm install -global http-server
npm run build
http-server dist -o
```
