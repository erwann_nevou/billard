export class Boule {
    static RAYON_BOULE = 2.85;
    static POURCENT_GARDER_VITESSE = 0.99;
    public rayon: number = Boule.RAYON_BOULE;
    public x: number;
    public y: number;
    public vx: number;
    public vy: number;
    public numero: number;
    public nbIterationsJusquaCollision = -1;
    public collisionHaut = false;
    public collisionBas = false;
    public collisionDroite = false;
    public collisionGauche = false;
    public collisionBoule: Boule | null = null;
    public xFutur = 0;
    public yFutur = 0;
    public trajectoireA = 0;
    public trajectoireB = 0;

    constructor(x: number, y: number, vx: number, vy: number, multiplicateur: number, numero: number) {
        this.x = x;
        this.y = y;
        this.vx = vx;
        this.vy = vy;
        this.numero = numero;
        this.rayon = Boule.RAYON_BOULE * multiplicateur;
    }

    draw(ctx: CanvasRenderingContext2D, couleur: string): void {
        // Affichage boule
        ctx.fillStyle = couleur;
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.rayon, 0, 2 * Math.PI);
        ctx.fill();
        ctx.closePath();
        ctx.fillStyle = "#000000";
        // ctx.fillText(`${this.numero}`, this.x, this.y);
    }

    update(): void {
        const multiplicateurV = Math.abs(this.vx) + Math.abs(this.vy) > 2 ? Boule.POURCENT_GARDER_VITESSE : 0;
        this.x += this.vx * 0.016;
        this.y += this.vy * 0.016;
        this.vx *= multiplicateurV;
        this.vy *= multiplicateurV;
    }

    isRouge(): boolean {
        return this.numero > 8;
    }

    isJaune(): boolean {
        return this.numero < 8;
    }

    isNoire(): boolean {
        return this.numero === 8;
    }

    isBlanche(): boolean {
        return this.numero === 16;
    }

    enMouvement(): boolean {
        return Math.abs(this.vx) + Math.abs(this.vy) > 0;
    }
}
