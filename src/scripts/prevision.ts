import { cloneDeep } from "lodash";
import { MULTIPLICATEUR, restitution_collision_bord } from "./app";
import { Boule } from "./boule";
import { BouleIteratif } from "./bouleIteratif";
import { Rectangle, VecteurDessin, Point, Collision } from "./models";
import { Table } from "./table";

export class Prevision {
    static P_G_V = 0.995;
    // Durée frame
    static D_F = 0.016;

    static calculArriveeVecteur(
        surface: Rectangle,
        boule: BouleIteratif,
        boules: BouleIteratif[],
        coordonnees: VecteurDessin[] = []
    ): VecteurDessin[] {
        const rBoule = BouleIteratif.RAYON_BOULE * MULTIPLICATEUR;
        const vx = boule.vx;
        const vy = boule.vy;
        const xDepart = boule.x;
        const yDepart = boule.y;

        // Calcul vecteur total
        // Si v === 0 alors retour operation log = -Infinity
        const nombreIterationVx = Math.max(0, Math.log(0.1 / Math.abs(vx)) / Math.log(Prevision.P_G_V));
        const nombreIterationVy = Math.max(0, Math.log(0.1 / Math.abs(vy)) / Math.log(Prevision.P_G_V));
        const sommeVx = vx * ((1 - Math.pow(Prevision.P_G_V, Math.ceil(nombreIterationVx))) / (1 - Prevision.P_G_V)) * Prevision.D_F;
        const sommeVy = vy * ((1 - Math.pow(Prevision.P_G_V, Math.ceil(nombreIterationVy))) / (1 - Prevision.P_G_V)) * Prevision.D_F;
        const xFutur = xDepart + sommeVx;
        const yFutur = yDepart + sommeVy;
        let collisionGauche = xFutur < surface.x + rBoule;
        let collisionDroite = xFutur > surface.x + surface.w - rBoule;
        let collisionHaut = yFutur < surface.y + rBoule;
        let collisionBas = yFutur > surface.y + surface.h - rBoule;
        // Si on pointe vers le haut le yCollision est la bordure haute sinon bordure basse
        const yCollision = surface.y + (collisionHaut ? rBoule : surface.h - rBoule);
        // Si on pointe vers la gauche le xCollision est la bordure gauche sinon bordure droite
        const xCollision = surface.x + (collisionGauche ? rBoule : surface.w - rBoule);

        // Calcul du x où on va heurter le haut ou le bas (y = a*x + b) => (x = (y - b) / a)
        // a = (Yb - Ya) / (Xb - Xa)
        // b = Ya - a * Xa
        const a = (yFutur - yDepart) / (xFutur - xDepart);
        const b = yDepart - a * xDepart;
        const xPointCollisionHautBas = (yCollision - b) / a;
        const yPointCollisionGaucheDroite = a * xCollision + b;

        if (collisionHaut && collisionGauche) {
            collisionHaut = xPointCollisionHautBas > surface.x + rBoule;
            collisionGauche = !collisionHaut;
        }

        if (collisionHaut && collisionDroite) {
            collisionHaut = xPointCollisionHautBas < surface.x + surface.w - rBoule;
            collisionDroite = !collisionHaut;
        }

        if (collisionBas && collisionGauche) {
            collisionBas = xPointCollisionHautBas > surface.x + rBoule;
            collisionGauche = !collisionBas;
        }

        if (collisionBas && collisionDroite) {
            collisionBas = xPointCollisionHautBas < surface.x + surface.w - rBoule;
            collisionDroite = !collisionBas;
        }

        let collisionBoule: BouleIteratif | null = null;
        let xCollisionBoule = -Infinity;
        let yCollisionBoule = -Infinity;
        let distanceCollisionBoule = Infinity;

        boules
            .filter((bl) => bl.numero !== boule.numero)
            .forEach((bouleCourante) => {
                // Equation du cercle de la boule
                // (x-Xcentre)² + (y-Ycentre)² = rayon²
                // Equation droite
                // y = ax + b

                // (x-Xcentre)² + (y - Ycentre)² = rayon²
                // x² - 2*x*Xcentre + Xcentre² + y² - 2y*Ycentre + Ycentre² - rayon² = 0
                // x² - 2*x*Xcentre + Xcentre² + (a*x + b)² - 2(a*x +b)*Ycentre + Ycentre² - rayon² = 0
                // x² - 2*x*Xcentre + Xcentre² + (a*x + b)² - 2*Ycentre*a*x - 2*Ycentre*b + Ycentre² - rayon² = 0
                // x² - 2*x*Xcentre + Xcentre² + (a*x)² + 2*a*x*b + b² - 2*Ycentre*a*x - 2*Ycentre*b + Ycentre² - rayon² = 0
                // x² - 2*x*Xcentre + Xcentre² + a²*x² + 2*a*x*b + b² - 2*Ycentre*a*x - 2*Ycentre*b + Ycentre² - rayon² = 0
                // x²*(1 + a²) + x*(-2*XCentre + 2*a*b - 2*Ycentre*a) + (Xcentre² + b² - 2*Ycentre*b + Ycentre² - rayon²) = 0

                // aD = 1 + a²
                // bD = -2*XCentre + 2*a*b - 2*Ycentre*a
                // cD = Xcentre² + b² - 2*Ycentre*b + Ycentre² - rayon²

                // D = bD² - 4*aD*cD
                // Si D < 0 alors pas de solution
                // Si D = 0 => x = -bD / (2*aD)
                // Si D > 0 => x1 = (-bD - Math.sqrt(D)) / (2*aD)
                //             x2 = (-bD + Math.sqrt(D)) / (2*aD)
                const aD = 1 + a * a;
                const bD = 2 * (a * b - bouleCourante.x - a * bouleCourante.y);
                const cD =
                    bouleCourante.x * bouleCourante.x +
                    b * b -
                    2 * b * bouleCourante.y +
                    bouleCourante.y * bouleCourante.y -
                    rBoule * 2 * rBoule * 2;
                const delta = Math.pow(bD, 2) - 4 * aD * cD;
                if (delta > 0) {
                    const x1 = (-bD - Math.sqrt(delta)) / (2 * aD);
                    const y1 = a * x1 + b;
                    const x2 = (-bD + Math.sqrt(delta)) / (2 * aD);
                    const y2 = a * x2 + b;
                    if (
                        (x1 >= Math.min(xDepart, xFutur) && x1 <= Math.max(xDepart, xFutur)) ||
                        (x2 >= Math.min(xDepart, xFutur) && x2 <= Math.max(xDepart, xFutur))
                    ) {
                        const distanceImpact1 = Math.pow(xDepart - x1, 2) + Math.pow(yDepart - y1, 2);
                        const distanceImpact2 = Math.pow(xDepart - x2, 2) + Math.pow(yDepart - y2, 2);
                        const pointImpact: Point = distanceImpact1 < distanceImpact2 ? { x: x1, y: y1 } : { x: x2, y: y2 };
                        const distancePIDepart = Math.min(distanceImpact1, distanceImpact2);
                        collisionHaut = false;
                        collisionBas = false;
                        collisionGauche = false;
                        collisionDroite = false;
                        if (distancePIDepart < distanceCollisionBoule) {
                            xCollisionBoule = pointImpact.x;
                            yCollisionBoule = pointImpact.y;
                            distanceCollisionBoule = distancePIDepart;
                            collisionBoule = bouleCourante;
                        }
                    }
                }
            });

        if (collisionHaut || collisionBas || collisionGauche || collisionDroite) {
            const collision = {
                x: collisionHaut || collisionBas ? xPointCollisionHautBas : xCollision,
                y: collisionGauche || collisionDroite ? yPointCollisionGaucheDroite : yCollision,
                haut: collisionHaut,
                bas: collisionBas,
                gauche: collisionGauche,
                droite: collisionDroite,
            };

            const nouvelleBoule = this.getBouleACollision(boule, collision);
            coordonnees.push({ depart: { x: xDepart, y: yDepart }, arrivee: { x: collision.x, y: collision.y }, dessinBoule: false });
            return Prevision.calculArriveeVecteur(surface, nouvelleBoule, boules, coordonnees);
        } else if (collisionBoule !== null) {
            const nouvelleBoule = cloneDeep(boule);
            nouvelleBoule.x = xCollisionBoule;
            nouvelleBoule.y = yCollisionBoule;
            const b1 = nouvelleBoule;
            const b2 = cloneDeep(<BouleIteratif>collisionBoule);
            const vCollision = { x: b2.x - b1.x, y: b2.y - b1.y };
            const distance = Math.sqrt((b1.x - b2.x) * (b1.x - b2.x) + (b1.y - b2.y) * (b1.y - b2.y));
            const vCollisionNorm = {
                x: vCollision.x / distance,
                y: vCollision.y / distance,
            };
            const vRelativeVelocity = { x: b1.vx - b2.vx, y: b1.vy - b2.vy };
            const speed = vRelativeVelocity.x * vCollisionNorm.x + vRelativeVelocity.y * vCollisionNorm.y;
            if (speed > 0) {
                b1.vx -= speed * vCollisionNorm.x;
                b1.vy -= speed * vCollisionNorm.y;
                b2.vx += speed * vCollisionNorm.x;
                b2.vy += speed * vCollisionNorm.y;
            }
            const nombreIterationVx1 = Math.max(0, Math.log(0.1 / Math.abs(b1.vx)) / Math.log(Prevision.P_G_V));
            const nombreIterationVy1 = Math.max(0, Math.log(0.1 / Math.abs(b1.vy)) / Math.log(Prevision.P_G_V));
            const sommeVx1 =
                b1.vx * ((1 - Math.pow(Prevision.P_G_V, Math.ceil(nombreIterationVx1))) / (1 - Prevision.P_G_V)) * Prevision.D_F;
            const sommeVy1 =
                b1.vy * ((1 - Math.pow(Prevision.P_G_V, Math.ceil(nombreIterationVy1))) / (1 - Prevision.P_G_V)) * Prevision.D_F;
            const nombreIterationVx2 = Math.max(0, Math.log(0.1 / Math.abs(b2.vx)) / Math.log(Prevision.P_G_V));
            const nombreIterationVy2 = Math.max(0, Math.log(0.1 / Math.abs(b2.vy)) / Math.log(Prevision.P_G_V));
            const sommeVx2 =
                b2.vx * ((1 - Math.pow(Prevision.P_G_V, Math.ceil(nombreIterationVx2))) / (1 - Prevision.P_G_V)) * Prevision.D_F;
            const sommeVy2 =
                b2.vy * ((1 - Math.pow(Prevision.P_G_V, Math.ceil(nombreIterationVy2))) / (1 - Prevision.P_G_V)) * Prevision.D_F;
            coordonnees.push({
                depart: { x: xDepart, y: yDepart },
                arrivee: { x: nouvelleBoule.x, y: nouvelleBoule.y },
                dessinBoule: true,
            });
            coordonnees.push({
                depart: { x: b2.x, y: b2.y },
                arrivee: { x: b2.x + sommeVx2 / 10, y: b2.y + sommeVy2 / 10 },
                dessinBoule: false,
            });
            coordonnees.push({
                depart: { x: b1.x, y: b1.y },
                arrivee: { x: b1.x + sommeVx1 / 10, y: b1.y + sommeVy1 / 10 },
                dessinBoule: false,
            });
        } else {
            coordonnees.push({ depart: { x: xDepart, y: yDepart }, arrivee: { x: xFutur, y: yFutur }, dessinBoule: true });
        }

        return coordonnees;
    }

    static getBouleACollision(boule: BouleIteratif, collision: Collision): BouleIteratif {
        const vx = boule.vx;
        const vy = boule.vy;
        const xDepart = boule.x;
        const yDepart = boule.y;
        const ignorerVx = vx >= -0.1 && vx <= 0.1;
        const ignorerVy = vy >= -0.1 && vy <= 0.1;

        // Calcul des Vx Vy à collision pour construire le vecteur de rebond
        // Calcul du nombre d'iterations jusqu'à collision
        // Somme Vx = Vx0 * ((1 - P_G_V ^ NbIter) / (1 - P_G_V))
        // Xcollision - XDepart = Vx0 * ((1 - P_G_V ^ NbIter) / (1 - P_G_V))
        // nbIterX = log((-1 * ((1 - P_G_V) * (Xcollision - XDepart) - Vx0)) / Vx0) / log(P_G_V)
        // nbIterY = log((-1 * ((1 - P_G_V) * (Ycollision - YDepart) - Vy0)) / Vy0) / log(P_G_V)
        const nbIterationJusquaCollisionVx =
            Math.log((-1 * (((1 - Prevision.P_G_V) * (collision.x - xDepart)) / Prevision.D_F - vx)) / vx) / Math.log(Prevision.P_G_V);
        const nbIterationJusquaCollisionVy =
            Math.log((-1 * (((1 - Prevision.P_G_V) * (collision.y - yDepart)) / Prevision.D_F - vy)) / vy) / Math.log(Prevision.P_G_V);

        // On applique le nombre d'itérations obtenu à Vx et Vy pour connaître leur valeur au point de collision
        const vxACollision = vx * Math.pow(Prevision.P_G_V, ignorerVx ? nbIterationJusquaCollisionVy : nbIterationJusquaCollisionVx);
        const vyACollision = vy * Math.pow(Prevision.P_G_V, ignorerVy ? nbIterationJusquaCollisionVx : nbIterationJusquaCollisionVy);

        const nouvelleBoule = cloneDeep(boule);
        // On construit le second vecteur post collision
        nouvelleBoule.vx = collision.gauche || collision.droite ? vxACollision * -restitution_collision_bord : vxACollision;
        nouvelleBoule.vy = collision.haut || collision.bas ? vyACollision * -restitution_collision_bord : vyACollision;
        nouvelleBoule.x = collision.x;
        nouvelleBoule.y = collision.y;
        return nouvelleBoule;
    }

    static calculNbIterationCollisionBouleImmobile(boule: Boule, boules: Boule[]): void {
        let bouleCollision: Boule | null = null;
        let nbIter = +Infinity;
        // Calcul de toutes les collisions possibles
        boules
            .filter((bl) => bl.numero !== boule.numero && !bl.enMouvement())
            .forEach((bouleCourante) => {
                if (!bouleCourante.enMouvement()) {
                    const vx = boule.vx;
                    const vy = boule.vy;
                    // Equation du cercle de la boule
                    // (x - Xcentre)² + (y - Ycentre)² = rayon²
                    // Equation droite
                    // y = ax + b

                    // (x - Xcentre)² + (y - Ycentre)² = rayon²
                    // x² - 2*x*Xcentre + Xcentre² + y² - 2y*Ycentre + Ycentre² - rayon² = 0
                    // x² - 2*x*Xcentre + Xcentre² + (a*x + b)² - 2(a*x +b)*Ycentre + Ycentre² - rayon² = 0
                    // x² - 2*x*Xcentre + Xcentre² + (a*x + b)² - 2*Ycentre*a*x - 2*Ycentre*b + Ycentre² - rayon² = 0
                    // x² - 2*x*Xcentre + Xcentre² + (a*x)² + 2*a*x*b + b² - 2*Ycentre*a*x - 2*Ycentre*b + Ycentre² - rayon² = 0
                    // x² - 2*x*Xcentre + Xcentre² + a²*x² + 2*a*x*b + b² - 2*Ycentre*a*x - 2*Ycentre*b + Ycentre² - rayon² = 0
                    // x²*(1 + a²) + x*(-2*XCentre + 2*a*b - 2*Ycentre*a) + (Xcentre² + b² - 2*Ycentre*b + Ycentre² - rayon²) = 0

                    // aD = 1 + a²
                    // bD = -2*XCentre + 2*a*b - 2*Ycentre*a
                    // cD = Xcentre² + b² - 2*Ycentre*b + Ycentre² - rayon²

                    // D = bD² - 4*aD*cD
                    // Si D < 0 alors pas de solution
                    // Si D = 0 => x = -bD / (2*aD)
                    // Si D > 0 => x1 = (-bD - Math.sqrt(D)) / (2*aD)
                    //             x2 = (-bD + Math.sqrt(D)) / (2*aD)
                    const aD = 1 + Math.pow(boule.trajectoireA, 2);
                    const bD = 2 * (boule.trajectoireA * boule.trajectoireB - bouleCourante.x - boule.trajectoireA * bouleCourante.y);
                    const cD =
                        Math.pow(bouleCourante.x, 2) +
                        Math.pow(boule.trajectoireB, 2) -
                        2 * boule.trajectoireB * bouleCourante.y +
                        Math.pow(bouleCourante.y, 2) -
                        Math.pow(bouleCourante.rayon * 2, 2);
                    const delta = Math.pow(bD, 2) - 4 * aD * cD;
                    if (delta > 0) {
                        const x1 = (-bD - Math.sqrt(delta)) / (2 * aD);
                        const y1 = boule.trajectoireA * x1 + boule.trajectoireB;
                        const x2 = (-bD + Math.sqrt(delta)) / (2 * aD);
                        const y2 = boule.trajectoireA * x2 + boule.trajectoireB;

                        if (
                            (x1 >= Math.min(boule.x, boule.xFutur) && x1 <= Math.max(boule.x, boule.xFutur)) ||
                            (x2 >= Math.min(boule.x, boule.xFutur) && x2 <= Math.max(boule.x, boule.xFutur))
                        ) {
                            const distanceImpact1 = Math.pow(boule.x - x1, 2) + Math.pow(boule.y - y1, 2);
                            const distanceImpact2 = Math.pow(boule.x - x2, 2) + Math.pow(boule.y - y2, 2);
                            const pointImpact: Point = distanceImpact1 < distanceImpact2 ? { x: x1, y: y1 } : { x: x2, y: y2 };
                            // console.log(`Point d'impact : ${boule.numero} sur ${bouleCourante.numero}`, pointImpact);
                            const ignorerVx = vx >= -0.1 && vx <= 0.1;
                            const ignorerVy = vy >= -0.1 && vy <= 0.1;
                            const nbIterationJusquaCollisionVx =
                                Math.log((-1 * (((1 - Prevision.P_G_V) * (pointImpact.x - boule.x)) / Prevision.D_F - vx)) / vx) /
                                Math.log(Prevision.P_G_V);
                            const nbIterationJusquaCollisionVy =
                                Math.log((-1 * (((1 - Prevision.P_G_V) * (pointImpact.y - boule.y)) / Prevision.D_F - vy)) / vy) /
                                Math.log(Prevision.P_G_V);
                            const nbIterationsJusquaCollision = ignorerVx
                                ? ignorerVy
                                    ? -1
                                    : nbIterationJusquaCollisionVy
                                : nbIterationJusquaCollisionVx;
                            if (nbIterationsJusquaCollision < nbIter) {
                                nbIter = nbIterationsJusquaCollision;
                                bouleCollision = bouleCourante;
                            }
                        }
                    }
                    if (bouleCollision) {
                        boule.collisionBoule = bouleCollision;
                        boule.nbIterationsJusquaCollision = nbIter;
                    }
                } else if (boule.trajectoireA !== bouleCourante.trajectoireA) {
                    // Calcul de l'intersection des droites
                    // a1*x + b1 = y
                    // a2*x + b2 = y
                    // x = (b2 - b1) / (a1 - a2)
                    // si a1 === a2 alors pas de solutions : droites paralleles
                    const xCollisionTrajectoire =
                        (bouleCourante.trajectoireB - boule.trajectoireB) / (boule.trajectoireA - bouleCourante.trajectoireA);
                    const yCollisionTrajectoire = boule.trajectoireA * xCollisionTrajectoire + boule.trajectoireB;
                    // Application rayon boule sur résultat
                    // (xCollisionTrajectoire - x)² + (yCollisionTrajectoire - y)² = (2*rayonBoule)²
                    // y = a1*x + b1
                    // xCollisionTrajectoire² - 2*xCollisionTrajectoire*x + x² + yCollisionTrajectoire² - 2*yCollisionTrajectoire*y + y² = (2*rayonBoule)²
                    // xCollisionTrajectoire² - 2*xCollisionTrajectoire*x + x² + yCollisionTrajectoire² - 2*yCollisionTrajectoire*(a1*x + b1) + (a1*x + b1)² = (2*rayonBoule)²
                    // xCollisionTrajectoire² - 2*xCollisionTrajectoire*x + x² + yCollisionTrajectoire² - 2*yCollisionTrajectoire*a1*x - 2*yCollisionTrajectoire*b1 + a1²*x² + 2*a1*x*b1 + b1² = (2*rayonBoule)²
                    // x²*(1 + a1²) + x(-2*xCollisionTrajectoire - 2*yCollisionTrajectoire*a1 + 2*a1*b1) + (xCollisionTrajectoire² + yCollisionTrajectoire² - 2*yCollisionTrajectoire*b1 + b1² - 4*rayonBoule²) = 0
                    // aD = 1 + a1²
                    // bD = -2*xCollisionTrajectoire - 2*yCollisionTrajectoire*a1 + 2*a1*b1
                }
            });
    }

    /**
     * Calcul du nombre d'itérations jusqu'à une collision avec le bord
     * @param surface Surface de jeu
     * @param boule Boule en mouvement
     */
    static calculNbIterationCollisionBords(surface: Rectangle, boule: Boule): void {
        let collisionGauche = boule.xFutur < surface.x + BouleIteratif.RAYON_BOULE;
        let collisionDroite = boule.xFutur > surface.x + surface.w - BouleIteratif.RAYON_BOULE;
        let collisionHaut = boule.yFutur < surface.y + BouleIteratif.RAYON_BOULE;
        let collisionBas = boule.yFutur > surface.y + surface.h - BouleIteratif.RAYON_BOULE;

        // Pas de collision avec les bords
        boule.nbIterationsJusquaCollision = -1;

        if (collisionHaut || collisionBas || collisionGauche || collisionDroite) {
            // Si on pointe vers le haut le yCollision est la bordure haute sinon bordure basse
            const yCollision = surface.y + (collisionHaut ? BouleIteratif.RAYON_BOULE : surface.h - BouleIteratif.RAYON_BOULE);
            // Si on pointe vers la gauche le xCollision est la bordure gauche sinon bordure droite
            const xCollision = surface.x + (collisionGauche ? BouleIteratif.RAYON_BOULE : surface.w - BouleIteratif.RAYON_BOULE);

            // Calcul du x où on va heurter le haut ou le bas (y = a*x + b) => (x = (y - b) / a)
            const xPointCollisionHautBas = (yCollision - boule.trajectoireB) / boule.trajectoireA;
            const yPointCollisionGaucheDroite = boule.trajectoireA * xCollision + boule.trajectoireB;

            if (collisionHaut && collisionGauche) {
                collisionHaut = xPointCollisionHautBas > surface.x + BouleIteratif.RAYON_BOULE;
                collisionGauche = !collisionHaut;
            }

            if (collisionHaut && collisionDroite) {
                collisionHaut = xPointCollisionHautBas < surface.x + surface.w - BouleIteratif.RAYON_BOULE;
                collisionDroite = !collisionHaut;
            }

            if (collisionBas && collisionGauche) {
                collisionBas = xPointCollisionHautBas > surface.x + BouleIteratif.RAYON_BOULE;
                collisionGauche = !collisionBas;
            }

            if (collisionBas && collisionDroite) {
                collisionBas = xPointCollisionHautBas < surface.x + surface.w - BouleIteratif.RAYON_BOULE;
                collisionDroite = !collisionBas;
            }

            const xCollisionCalcul = collisionHaut || collisionBas ? xPointCollisionHautBas : xCollision;
            const yCollisionCalcul = collisionGauche || collisionDroite ? yPointCollisionGaucheDroite : yCollision;
            const ignorerVx = boule.vx >= -0.1 && boule.vx <= 0.1;
            const ignorerVy = boule.vy >= -0.1 && boule.vy <= 0.1;
            const nbIterationJusquaCollisionVx =
                Math.log((-1 * (((1 - Prevision.P_G_V) * (xCollisionCalcul - boule.x)) / Prevision.D_F - boule.vx)) / boule.vx) /
                Math.log(Prevision.P_G_V);
            const nbIterationJusquaCollisionVy =
                Math.log((-1 * (((1 - Prevision.P_G_V) * (yCollisionCalcul - boule.y)) / Prevision.D_F - boule.vy)) / boule.vy) /
                Math.log(Prevision.P_G_V);
            boule.collisionBas = collisionBas;
            boule.collisionHaut = collisionHaut;
            boule.collisionDroite = collisionDroite;
            boule.collisionGauche = collisionGauche;
            boule.nbIterationsJusquaCollision = ignorerVx ? (ignorerVy ? -1 : nbIterationJusquaCollisionVy) : nbIterationJusquaCollisionVx;
        }
    }

    static calculProchainEtat(ctx: CanvasRenderingContext2D, table: Table, surface: Rectangle, boules: Boule[]): Boule[] {
        ctx.clearRect(0, 0, 1800, 900);
        table.draw(ctx);
        boules.forEach((b) => {
            const couleur: string = b.numero === 16 ? "#FFFFFF" : b.isNoire() ? "#000000" : b.isJaune() ? "#FFFF00" : "#FF0000";
            b.draw(ctx, couleur);
        });

        const nouvellesBoules: Boule[] = cloneDeep(boules);
        // Calcul du nombre d'itérations avant prochaine collision
        let nbIterationsJusquaProchaineCollision = +Infinity;
        // Calcul de la trajectoire pour chaque boule
        nouvellesBoules.forEach((boule) => {
            const vx = boule.vx;
            const vy = boule.vy;
            // Calcul vecteur total
            // Si v === 0 alors retour operation log = -Infinity
            const nombreIterationVx = Math.max(0, Math.log(0.1 / Math.abs(vx)) / Math.log(Prevision.P_G_V));
            const nombreIterationVy = Math.max(0, Math.log(0.1 / Math.abs(vy)) / Math.log(Prevision.P_G_V));
            const sommeVx = vx * ((1 - Math.pow(Prevision.P_G_V, Math.ceil(nombreIterationVx))) / (1 - Prevision.P_G_V)) * Prevision.D_F;
            const sommeVy = vy * ((1 - Math.pow(Prevision.P_G_V, Math.ceil(nombreIterationVy))) / (1 - Prevision.P_G_V)) * Prevision.D_F;
            boule.xFutur = boule.x + sommeVx;
            boule.yFutur = boule.y + sommeVy;
            // a = (Yb - Ya) / (Xb - Xa)
            // b = Ya - a * Xa
            boule.trajectoireA = (boule.yFutur - boule.y) / (boule.xFutur - boule.x);
            boule.trajectoireB = boule.y - boule.trajectoireA * boule.x;
        });
        // Pour chaque boule en mouvement : calcul des collisions à venir
        nouvellesBoules
            .filter((b) => b.enMouvement())
            .forEach((boule) => {
                this.calculNbIterationCollisionBouleImmobile(boule, boules);
                if (!boule.collisionBoule) {
                    this.calculNbIterationCollisionBords(surface, boule);
                }
                if (boule.nbIterationsJusquaCollision > -1) {
                    nbIterationsJusquaProchaineCollision = Math.min(
                        nbIterationsJusquaProchaineCollision,
                        boule.nbIterationsJusquaCollision
                    );
                }
            });
        // On reset les collisions des boules qui ne rentrent pas en collision à la prochaine étape
        nouvellesBoules
            .filter((b) => b.enMouvement())
            .forEach((boule) => {
                if (
                    boule.nbIterationsJusquaCollision !== -1 &&
                    boule.nbIterationsJusquaCollision !== nbIterationsJusquaProchaineCollision
                ) {
                    boule.collisionBas = false;
                    boule.collisionDroite = false;
                    boule.collisionGauche = false;
                    boule.collisionHaut = false;
                    boule.collisionBoule = null;
                }
            });
        // Je fais avancer chaque boule du nombre d'iterations
        nouvellesBoules
            .filter((b) => b.enMouvement())
            .forEach((boule) => {
                const multiplicateurVecteur =
                    ((1 - Math.pow(Prevision.P_G_V, Math.ceil(nbIterationsJusquaProchaineCollision))) / (1 - Prevision.P_G_V)) *
                    Prevision.D_F;
                boule.x += boule.vx * multiplicateurVecteur;
                boule.y += boule.vy * multiplicateurVecteur;
                // On applique le nombre d'itérations obtenu à Vx et Vy pour connaître leur valeur au point de collision
                boule.vx *= Math.pow(Prevision.P_G_V, nbIterationsJusquaProchaineCollision);
                boule.vy *= Math.pow(Prevision.P_G_V, nbIterationsJusquaProchaineCollision);
            });
        // J'applique les collisions
        nouvellesBoules
            .filter((b) => b.enMouvement())
            .forEach((boule) => {
                if (boule.collisionGauche || boule.collisionDroite) boule.vx *= -restitution_collision_bord;
                if (boule.collisionHaut || boule.collisionBas) boule.vy *= -restitution_collision_bord;
                if (boule.collisionBoule) {
                    const b1 = boule;
                    const b2 = nouvellesBoules.filter((b) => b.numero === boule.collisionBoule?.numero)[0];
                    const vCollision = { x: b2.x - b1.x, y: b2.y - b1.y };
                    const distance = Math.sqrt((b1.x - b2.x) * (b1.x - b2.x) + (b1.y - b2.y) * (b1.y - b2.y));
                    const vCollisionNorm = {
                        x: vCollision.x / distance,
                        y: vCollision.y / distance,
                    };
                    const vRelativeVelocity = { x: b1.vx - b2.vx, y: b1.vy - b2.vy };
                    const speed = vRelativeVelocity.x * vCollisionNorm.x + vRelativeVelocity.y * vCollisionNorm.y;
                    if (speed > 0) {
                        b1.vx -= speed * vCollisionNorm.x;
                        b1.vy -= speed * vCollisionNorm.y;
                        b2.vx += speed * vCollisionNorm.x;
                        b2.vy += speed * vCollisionNorm.y;
                    }
                }
                boule.collisionBoule = null;
                boule.collisionBas = false;
                boule.collisionDroite = false;
                boule.collisionGauche = false;
                boule.collisionHaut = false;
            });
        return nouvellesBoules;
    }

    static getEtatsTour(
        ctx: CanvasRenderingContext2D,
        table: Table,
        surface: Rectangle,
        boules: Boule[],
        etats: Boule[][] = []
    ): Boule[][] {
        etats.push(boules);
        const etatEnMouvement = boules.reduce((acc, boule) => (acc ||= boule.enMouvement()), false);
        if (etatEnMouvement) {
            return this.getEtatsTour(ctx, table, surface, this.calculProchainEtat(ctx, table, surface, boules), etats);
        }
        return etats;
    }
}
