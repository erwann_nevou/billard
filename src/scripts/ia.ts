import { cloneDeep } from "lodash";
import { Etat, Score } from "./etat";
import { Rectangle, Cercle, Vecteur } from "./models";

export class IA {
    static calculTour(etat: Etat, surface: Rectangle, restitution_collision_bord: number, coins: Cercle[]): Vecteur {
        const copieEtat = cloneDeep(etat);
        copieEtat.tourSuivant();
        const t1 = performance.now();
        const boulesRouges = etat.getBoules().filter((b) => b.numero > 8 && b.numero !== 16);
        const bouleBlanche = etat.getBouleBlanche();
        if (boulesRouges.length === 0) {
            boulesRouges.push(...etat.getBoules().filter((b) => b.numero === 8));
        }
        const vecteurs = boulesRouges.map((b) => {
            const vx = b.x - bouleBlanche.x;
            const vy = b.y - bouleBlanche.y;
            const longueurVecteur = Math.sqrt(vx * vx + vy * vy);
            return { num: b.numero, vx: (vx / longueurVecteur) * 1000, vy: (vy / longueurVecteur) * 1000 };
        });
        const etatsFinaux = vecteurs.map((v) => {
            const nouvelEtat = cloneDeep(copieEtat);
            nouvelEtat.getBouleBlanche().vx = v.vx;
            nouvelEtat.getBouleBlanche().vy = v.vy;
            const etatsFinaux = IA.calculEtats([nouvelEtat], surface, restitution_collision_bord, coins);
            const etatFinal = etatsFinaux[etatsFinaux.length - 1];
            const scoreEtatFinal = IA.evaluerEtat(etatFinal, etat.getScore(), v);
            return { v, etatFinal, scoreEtatFinal };
        });
        console.log(etatsFinaux);
        etatsFinaux.sort((a, b) => b.scoreEtatFinal - a.scoreEtatFinal);
        const t2 = performance.now();
        console.log(`Total tour IA : ${t2 - t1}`);
        return etatsFinaux[0].v;
    }

    static evaluerEtat(etat: Etat, scorePrecedent: Score, v: { num: number; vx: number; vy: number }): number {
        const aFaute = etat.aFaute(scorePrecedent);
        const nouveauScore = etat.getScore();
        const poidsFaute = -1;
        const poidsPointRouge = 2;
        const poidsPointJaune = -1;
        const totalFaute = aFaute ? poidsFaute : 0;
        const totalPointsRouge = (nouveauScore.rouge - scorePrecedent.rouge) * poidsPointRouge;
        const totalPointsJaune = (nouveauScore.jaune - scorePrecedent.jaune) * poidsPointJaune;
        return totalFaute + totalPointsRouge + totalPointsJaune;
    }

    static calculEtats = (etatsPrecedents: Etat[], surface: Rectangle, restitution_collision_bord: number, coins: Cercle[]): Etat[] => {
        const prochainEtat = etatsPrecedents[etatsPrecedents.length - 1].getProchainEtatNonDeep(surface, restitution_collision_bord, coins);
        if (prochainEtat !== null) {
            return IA.calculEtats([...etatsPrecedents, prochainEtat], surface, restitution_collision_bord, coins);
        }
        return etatsPrecedents;
    };
}
