export interface Dimension {
    id: number;
    table: { h: number; w: number };
    surface: { h: number; w: number };
}

export interface Rectangle extends Point {
    w: number;
    h: number;
}

export interface Cercle extends Point {
    rayon: number;
}

export interface Point {
    x: number;
    y: number;
}

export interface Vecteur {
    vx: number;
    vy: number;
}

export interface Collision extends Point {
    haut: boolean;
    bas: boolean;
    gauche: boolean;
    droite: boolean;
}

export interface VecteurDessin {
    depart: Point;
    arrivee: Point;
    dessinBoule: boolean;
}
