export class BouleIteratif {
    static RAYON_BOULE = 2.85;
    static POURCENT_GARDER_VITESSE = 0.995;
    public x: number;
    public y: number;
    public vx: number;
    public vy: number;
    public numero: number;

    constructor(x: number, y: number, vx: number, vy: number, numero: number) {
        this.x = x;
        this.y = y;
        this.vx = vx;
        this.vy = vy;
        this.numero = numero;
    }

    draw(ctx: CanvasRenderingContext2D, couleur: string, multiplicateur: number): void {
        ctx.fillStyle = couleur;
        ctx.beginPath();
        ctx.arc(this.x, this.y, BouleIteratif.RAYON_BOULE * multiplicateur, 0, 2 * Math.PI);
        ctx.fill();
        ctx.stroke();
        ctx.closePath();
    }

    update(): void {
        const multiplicateurV = Math.abs(this.vx) + Math.abs(this.vy) > 4 ? BouleIteratif.POURCENT_GARDER_VITESSE : 0;
        this.x += this.vx * 0.016;
        this.y += this.vy * 0.016;
        this.vx *= multiplicateurV;
        this.vy *= multiplicateurV;
    }

    isRouge(): boolean {
        return this.numero > 8;
    }

    isJaune(): boolean {
        return this.numero < 8;
    }

    isNoire(): boolean {
        return this.numero === 8;
    }

    isBlanche(): boolean {
        return this.numero === 16;
    }

    enMouvement(): boolean {
        return Math.abs(this.vx) + Math.abs(this.vy) > 0;
    }
}
