import { cloneDeep } from "lodash";
import { BouleIteratif } from "./bouleIteratif";
import { Etat, Joueur, Score } from "./etat";
import { Rectangle, Cercle, Vecteur } from "./models";

export class IA3 {
    static calculTour(joueur: Joueur, etat: Etat, surface: Rectangle, restitution_collision_bord: number, coins: Cercle[]): Vecteur {
        const copieEtat = cloneDeep(etat);
        copieEtat.tourSuivant();
        const t1 = performance.now();
        const boules = etat.getBoules().filter((b) => b.numero !== 16);
        const bouleBlanche = etat.getBouleBlanche();
        if (boules.length === 0) {
            boules.push(...boules.filter((b) => b.numero === 8));
        }
        const vecteurs = boules
            .map((boule) => {
                const vecteursBoule = [];
                const vx = boule.x - bouleBlanche.x;
                const vy = boule.y - bouleBlanche.y;
                const longueurVecteur = Math.sqrt(vx * vx + vy * vy);
                vecteursBoule.push({ num: boule.numero, vx: (vx / longueurVecteur) * 1000, vy: (vy / longueurVecteur) * 1000 });
                // a = (Yb - Ya) / (Xb - Xa)
                // b = Ya - a * Xa
                const a = vy / vx;
                const aP = -1 / a;
                const bP = boule.y - aP * boule.x;
                const aD = 1 + aP * aP;
                const bD = 2 * (aP * bP - boule.x - aP * boule.y);
                const rayonACalculer = [
                    BouleIteratif.RAYON_BOULE,
                    BouleIteratif.RAYON_BOULE / 5,
                    (2 * BouleIteratif.RAYON_BOULE) / 5,
                    (3 * BouleIteratif.RAYON_BOULE) / 5,
                    (4 * BouleIteratif.RAYON_BOULE) / 5,
                    (6 * BouleIteratif.RAYON_BOULE) / 5,
                    (7 * BouleIteratif.RAYON_BOULE) / 5,
                    (8 * BouleIteratif.RAYON_BOULE) / 5,
                    (9 * BouleIteratif.RAYON_BOULE) / 5,
                ];
                // (x - XCentre)² + (y - Ycentre)² = rayon²
                // aD = 1 + a²
                // bD = -2*XCentre + 2*a*b - 2*Ycentre*a
                // cD = Xcentre² + b² - 2*Ycentre*b + Ycentre² - rayon²
                rayonACalculer.forEach((r) => {
                    const cD = boule.x * boule.x + bP * bP - 2 * bP * boule.y + boule.y * boule.y - r * r;
                    const delta = bD * bD - 4 * aD * cD;
                    if (delta > 0) {
                        const x1 = (-bD - Math.sqrt(delta)) / (2 * aD);
                        const y1 = aP * x1 + bP;
                        const x2 = (-bD + Math.sqrt(delta)) / (2 * aD);
                        const y2 = aP * x2 + bP;
                        const vx1 = x1 - bouleBlanche.x;
                        const vy1 = y1 - bouleBlanche.y;
                        const vx2 = x2 - bouleBlanche.x;
                        const vy2 = y2 - bouleBlanche.y;
                        const longueurVecteur1 = Math.sqrt(vx1 * vx1 + vy1 * vy1);
                        const longueurVecteur2 = Math.sqrt(vx2 * vx2 + vy2 * vy2);
                        vecteursBoule.push({ num: boule.numero, vx: (vx1 / longueurVecteur1) * 1000, vy: (vy1 / longueurVecteur1) * 1000 });
                        vecteursBoule.push({ num: boule.numero, vx: (vx2 / longueurVecteur2) * 1000, vy: (vy2 / longueurVecteur2) * 1000 });
                    }
                });
                return vecteursBoule;
            })
            .flat();
        const etatsFinaux = vecteurs.map((v) => {
            const nouvelEtat = cloneDeep(copieEtat);
            nouvelEtat.getBouleBlanche().vx = v.vx;
            nouvelEtat.getBouleBlanche().vy = v.vy;
            const etatsFinaux = IA3.calculEtats([nouvelEtat], surface, restitution_collision_bord, coins);
            const etatFinal = etatsFinaux[etatsFinaux.length - 1];
            const scoreEtatFinal = IA3.evaluerEtat(joueur, etatFinal, etat.getScore());
            return { v, etatFinal, scoreEtatFinal };
        });
        etatsFinaux.sort((a, b) => b.scoreEtatFinal - a.scoreEtatFinal);
        console.log(etatsFinaux[0], etatsFinaux[etatsFinaux.length - 1]);
        const t2 = performance.now();
        console.log(`Total tour IA : ${t2 - t1}`);
        return etatsFinaux[0].v;
    }

    static evaluerEtat(joueur: Joueur, etat: Etat, scorePrecedent: Score): number {
        const aFaute = etat.aFaute(scorePrecedent);
        const nouveauScore = etat.getScore();
        const partieFinie = etat.isPartieFinie();
        const joueurVainqueur =
            (etat.joueur === "Jaune" && nouveauScore.jaune === 7) || (etat.joueur === "Rouge" && nouveauScore.rouge === 7 && !aFaute);
        const poidsFaute = -1;
        const poidsPointRouge = joueur === "Rouge" ? 2 : -2;
        const poidsPointJaune = joueur === "Jaune" ? 2 : -2;
        const poidsVictoire = 1000;
        const poidsDefaite = -1000;
        const totalFaute = aFaute ? poidsFaute : 0;
        const totalPointsRouge = (nouveauScore.rouge - scorePrecedent.rouge) * poidsPointRouge;
        const totalPointsJaune = (nouveauScore.jaune - scorePrecedent.jaune) * poidsPointJaune;
        const totalFin = partieFinie ? (joueurVainqueur ? poidsVictoire : poidsDefaite) : 0;
        return totalFaute + totalPointsRouge + totalPointsJaune + totalFin;
    }

    static calculEtats = (etatsPrecedents: Etat[], surface: Rectangle, restitution_collision_bord: number, coins: Cercle[]): Etat[] => {
        const prochainEtat = etatsPrecedents[etatsPrecedents.length - 1].getProchainEtatNonDeep(surface, restitution_collision_bord, coins);
        if (prochainEtat !== null) {
            return IA3.calculEtats([...etatsPrecedents, prochainEtat], surface, restitution_collision_bord, coins);
        }
        return etatsPrecedents;
    };
}
