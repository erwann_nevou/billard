import { cloneDeep } from "lodash";
import { dimensions, imgJoueur, imgOrdinateur, joueurHumain, MULTIPLICATEUR, origine } from "./app";
import { BouleIteratif } from "./bouleIteratif";
import { Rectangle, Cercle } from "./models";

export type Joueur = "Rouge" | "Jaune";

export interface Score {
    rouge: number;
    jaune: number;
}

export class Etat {
    public joueur: Joueur;
    public contactBordure: boolean;
    public nbTour: number;
    private boules: BouleIteratif[];
    private numero: number;
    private faute: boolean;
    private premierContact: number | null;

    constructor(
        numero: number,
        boules: BouleIteratif[],
        joueur: Joueur,
        nbTour: number,
        faute = false,
        premierContact: number | null = null,
        contactBordure = false,
        totalDetectionTrous = 0,
        totalUpdate = 0,
        totalDetectionCollision = 0,
        totalMvtTotal = 0,
        totalClone = 0,
        totalProchainEtat = 0
    ) {
        this.numero = numero;
        this.boules = boules;
        this.joueur = joueur;
        this.faute = faute;
        this.premierContact = premierContact;
        this.nbTour = nbTour;
        this.contactBordure = contactBordure;
    }

    getBouleBlanche(): BouleIteratif {
        return this.boules.filter((b) => b.isBlanche())[0];
    }

    draw(ctx: CanvasRenderingContext2D): void {
        const tailleTexteFrame = Math.max(10, 3 * MULTIPLICATEUR);
        ctx.font = `${tailleTexteFrame}px arial`;
        ctx.fillStyle = "black";
        ctx.fillText(`Frame : ${this.numero}`, 10, 10 + tailleTexteFrame);
        ctx.fillStyle = "white";

        // Dessin joueurs
        ctx.drawImage(imgJoueur, origine.x, 30, 10 * MULTIPLICATEUR, 10 * MULTIPLICATEUR);
        ctx.drawImage(imgOrdinateur, origine.x + (dimensions.table.w - 10) * MULTIPLICATEUR, 30, 10 * MULTIPLICATEUR, 10 * MULTIPLICATEUR);
        const centreImgJoueur = origine.x + 5 * MULTIPLICATEUR;
        const centreImgOrdinateur = origine.x + (dimensions.table.w - 5) * MULTIPLICATEUR;
        ctx.beginPath();
        ctx.arc(
            this.joueur === joueurHumain ? centreImgJoueur : centreImgOrdinateur,
            30 + 5 * MULTIPLICATEUR,
            10 * MULTIPLICATEUR,
            0,
            2 * Math.PI
        );
        ctx.stroke();
        ctx.closePath();

        // Dessin points
        const score = this.getScore();
        const couleurPointsJoueur = joueurHumain === "Jaune" ? "yellow" : "red";
        const couleurPointsJoueurTransparents = joueurHumain === "Jaune" ? "#ffffe6" : "#ffe6e6";
        const couleurPointsOrdinateur = joueurHumain === "Jaune" ? "red" : "yellow";
        const couleurPointsOrdinateurTransparents = joueurHumain === "Jaune" ? "#ffe6e6" : "#ffffe6";
        for (let i = 0; i <= 6; i++) {
            ctx.beginPath();
            ctx.arc(
                centreImgJoueur + 20 * MULTIPLICATEUR + i * 6 * MULTIPLICATEUR,
                30 + 5 * MULTIPLICATEUR,
                2 * MULTIPLICATEUR,
                0,
                2 * Math.PI
            );
            ctx.fillStyle =
                i < (joueurHumain === "Jaune" ? score.jaune : score.rouge) ? couleurPointsJoueur : couleurPointsJoueurTransparents;
            ctx.fill();
            ctx.stroke();
            ctx.closePath();
        }
        for (let i = 0; i <= 6; i++) {
            ctx.beginPath();
            ctx.arc(
                centreImgOrdinateur - 20 * MULTIPLICATEUR - i * 6 * MULTIPLICATEUR,
                30 + 5 * MULTIPLICATEUR,
                2 * MULTIPLICATEUR,
                0,
                2 * Math.PI
            );
            ctx.fillStyle =
                i < (joueurHumain === "Jaune" ? score.rouge : score.jaune) ? couleurPointsOrdinateur : couleurPointsOrdinateurTransparents;
            ctx.fill();
            ctx.stroke();
            ctx.closePath();
        }

        this.boules.forEach((b) => {
            const couleur: string = b.isBlanche() ? "white" : b.isNoire() ? "black" : b.isJaune() ? "yellow" : "red";
            b.draw(ctx, couleur, MULTIPLICATEUR);
        });
    }

    getProchainEtat(surface: Rectangle, restitution_collision_bord: number, coins: Cercle[]): Etat | null {
        if (this.getMouvementTotal() > 0) {
            const t0 = performance.now();
            const prochainesBoules = cloneDeep(this.boules);
            this.detectionsTrous(prochainesBoules, coins);
            prochainesBoules.forEach((b) => b.update());
            this.detectionCollisions(prochainesBoules, surface, restitution_collision_bord);
            return new Etat(
                this.numero + 1,
                prochainesBoules,
                this.joueur,
                this.nbTour,
                this.faute,
                this.premierContact,
                this.contactBordure
            );
        } else {
            return null;
        }
    }

    // 3 x plus rapide
    getProchainEtatNonDeep(surface: Rectangle, restitution_collision_bord: number, coins: Cercle[]): Etat | null {
        if (this.getMouvementTotal() > 0) {
            const prochainesBoules = this.boules;
            this.detectionsTrous(prochainesBoules, coins);
            prochainesBoules.forEach((b) => b.update());
            this.detectionCollisionsNonDeep(prochainesBoules, surface, restitution_collision_bord);
            return new Etat(
                this.numero + 1,
                prochainesBoules,
                this.joueur,
                this.nbTour,
                this.faute,
                this.premierContact,
                this.contactBordure
            );
        } else {
            return null;
        }
    }

    getMouvementTotal(): number {
        const mvt = this.boules.map((b) => Math.abs(b.vx) + Math.abs(b.vy)).reduce((acc, cur) => acc + cur);
        return mvt;
    }

    detectionsTrous(boules: BouleIteratif[], coins: Cercle[]): void {
        boules.forEach((b, i) => {
            if (b.enMouvement() && b.numero !== 16) {
                coins.forEach((c) => {
                    if (Etat.collisionCercle(b, c, c.rayon, 0)) {
                        boules.splice(i, 1);
                    }
                });
            }
        });
    }

    detectionCollisions(boules: BouleIteratif[], surface: Rectangle, restitution_collision_bord: number): void {
        let b1: BouleIteratif;
        let b2: BouleIteratif;
        // Start checking for collisions
        for (let i = 0; i < boules.length; i++) {
            b1 = boules[i];
            this.collisionBord(b1, surface, restitution_collision_bord);
            for (let j = i + 1; j < boules.length; j++) {
                b2 = boules[j];
                if ((b1.enMouvement() || b2.enMouvement()) && Etat.collisionCercle(b1, b2)) {
                    // FAUTE_BOULE_ADVERSE_TOUCHEE_PREMIER
                    if (!this.premierContact && b1.isBlanche()) {
                        this.premierContact = b2.numero;
                    }
                    if (!this.premierContact && b2.isBlanche()) {
                        this.premierContact = b1.numero;
                    }
                    const vCollision = { x: b2.x - b1.x, y: b2.y - b1.y };
                    const distance = Math.sqrt((b1.x - b2.x) * (b1.x - b2.x) + (b1.y - b2.y) * (b1.y - b2.y));
                    const vCollisionNorm = {
                        x: vCollision.x / distance,
                        y: vCollision.y / distance,
                    };
                    const vRelativeVelocity = { x: b1.vx - b2.vx, y: b1.vy - b2.vy };
                    const speed = vRelativeVelocity.x * vCollisionNorm.x + vRelativeVelocity.y * vCollisionNorm.y;
                    if (speed > 0) {
                        b1.vx -= speed * vCollisionNorm.x;
                        b1.vy -= speed * vCollisionNorm.y;
                        b2.vx += speed * vCollisionNorm.x;
                        b2.vy += speed * vCollisionNorm.y;
                    }
                }
            }
        }
    }

    detectionCollisionsNonDeep(boules: BouleIteratif[], surface: Rectangle, restitution_collision_bord: number): void {
        let b1: BouleIteratif;
        let b2: BouleIteratif;
        // Start checking for collisions
        for (let i = 0; i < boules.length; i++) {
            b1 = boules[i];
            this.collisionBord(b1, surface, restitution_collision_bord);
            for (let j = i + 1; j < boules.length; j++) {
                b2 = boules[j];
                if ((b1.enMouvement() || b2.enMouvement()) && Etat.collisionCercle(b1, b2)) {
                    // FAUTE_BOULE_ADVERSE_TOUCHEE_PREMIER
                    if (!this.premierContact && b1.isBlanche()) {
                        this.premierContact = b2.numero;
                    }
                    if (!this.premierContact && b2.isBlanche()) {
                        this.premierContact = b1.numero;
                    }
                    const vCollision = { x: b2.x - b1.x, y: b2.y - b1.y };
                    const distance = Math.sqrt((b1.x - b2.x) * (b1.x - b2.x) + (b1.y - b2.y) * (b1.y - b2.y));
                    const vCollisionNorm = {
                        x: vCollision.x / distance,
                        y: vCollision.y / distance,
                    };
                    const vRelativeVelocity = { x: b1.vx - b2.vx, y: b1.vy - b2.vy };
                    const speed = vRelativeVelocity.x * vCollisionNorm.x + vRelativeVelocity.y * vCollisionNorm.y;
                    if (speed > 0) {
                        b1.vx -= speed * vCollisionNorm.x;
                        b1.vy -= speed * vCollisionNorm.y;
                        b2.vx += speed * vCollisionNorm.x;
                        b2.vy += speed * vCollisionNorm.y;
                    }
                }
            }
        }
    }

    getJoueurOppose(): Joueur {
        return this.joueur === "Jaune" ? "Rouge" : "Jaune";
    }

    getBoules(): BouleIteratif[] {
        return this.boules;
    }

    changementJoueur(): void {
        this.joueur = this.getJoueurOppose();
    }

    tourSuivant(): void {
        this.nbTour++;
        this.faute = false;
        this.premierContact = null;
        this.contactBordure = false;
    }

    getScore(): Score {
        const scoreJaune = 7 - this.boules.filter((b) => b.numero < 8).length;
        const scoreRouge = 7 - this.boules.filter((b) => b.numero > 8).length + 1;
        return { rouge: scoreRouge, jaune: scoreJaune };
    }

    aFaute(scorePrecedent: Score): boolean {
        // Fautes
        // FAUTE_1ER_TOUR : Au premier tour le joueur change forcément
        // FAUTE_NOIRE_EMPOCHEE : La bille noire est empochée avant que le joueur ait rentré toutes les billes de sa série.
        // FAUTE_PAS_DE_BOULE_TOUCHEE : Si on ne touche pas de boule
        // FAUTE_BOULE_ADVERSE_TOUCHEE_PREMIER : Si on ne touche pas une bille de son groupe en premier.
        // FAUTE_BOULE_NOIRE_TOUCHEE_PREMIER : Si on touche la noire en premier contact.
        // FAUTE_COUP_NON_LEGAL : À chaque coup joué, le joueur doit frapper une bille de son groupe en 1er et soit :
        //   - Empocher une bille
        //   - Faire en sorte que la blanche ou une bille de son groupe touche la bande après le 1er contact
        const score = this.getScore();
        // FAUTE_1ER_TOUR
        let faute = this.nbTour === 1;
        // FAUTE_NOIRE_EMPOCHEE
        const billeNoireEmpochee = this.isPartieFinie();
        const joueurAToutRentre = (this.joueur === "Jaune" && score.jaune === 7) || (this.joueur === "Rouge" && score.rouge === 7);
        faute ||= billeNoireEmpochee && !joueurAToutRentre;
        // FAUTE_PAS_DE_BOULE_TOUCHEE ou FAUTE_BOULE_NOIRE_TOUCHEE_PREMIER
        faute ||= this.premierContact === null || (!billeNoireEmpochee && this.premierContact === 8);
        // FAUTE_BOULE_ADVERSE_TOUCHEE_PREMIER
        faute ||=
            this.premierContact !== null &&
            ((this.joueur === "Jaune" && this.premierContact > 8) || (this.joueur === "Rouge" && this.premierContact < 8));
        // FAUTE_COUP_NON_LEGAL : Pas de billes empochees && Pas de contact bordure
        faute ||= scorePrecedent.rouge === score.rouge && scorePrecedent.jaune === score.jaune && !this.contactBordure;
        return faute;
    }

    isPartieFinie(): boolean {
        return this.boules.filter((b) => b.numero === 8).length === 0;
    }

    collisionBord(b: BouleIteratif, surface: Rectangle, restitution_collision_bord: number): void {
        const rBoule = BouleIteratif.RAYON_BOULE * MULTIPLICATEUR;
        const minX = surface.x;
        const maxX = minX + surface.w;
        const minY = surface.y;
        const maxY = minY + surface.h;
        const collisionGauche = b.x - rBoule < minX;
        const collisionDroite = b.x + rBoule > maxX;
        const collisionHaut = b.y < minY + rBoule;
        const collisionBas = b.y + rBoule > maxY;
        // Gauche
        if (collisionGauche) {
            b.vx = Math.abs(b.vx) * restitution_collision_bord;
            b.x = minX + rBoule;
        }
        // Droite
        else if (collisionDroite) {
            b.vx = -Math.abs(b.vx) * restitution_collision_bord;
            b.x = maxX - rBoule;
        }

        // Haut
        if (collisionHaut) {
            b.vy = Math.abs(b.vy) * restitution_collision_bord;
            b.y = minY + rBoule;
        }
        // Bas
        else if (collisionBas) {
            b.vy = -Math.abs(b.vy) * restitution_collision_bord;
            b.y = maxY - rBoule;
        }

        const score = this.getScore();
        const joueurAToutRentre = (this.joueur === "Jaune" && score.jaune === 7) || (this.joueur === "Rouge" && score.rouge === 7);

        if (
            (collisionGauche || collisionDroite || collisionHaut || collisionBas) &&
            (b.numero === 16 ||
                (this.joueur === "Jaune" && b.numero < 8) ||
                (this.joueur === "Rouge" && b.numero > 8) ||
                (joueurAToutRentre && b.numero === 8)) &&
            this.premierContact !== null
        ) {
            this.contactBordure = true;
        }
    }

    static collisionCercle(
        c1: Cercle | BouleIteratif,
        c2: Cercle | BouleIteratif,
        r2 = BouleIteratif.RAYON_BOULE * MULTIPLICATEUR,
        r1 = BouleIteratif.RAYON_BOULE * MULTIPLICATEUR
    ): boolean {
        const distance = Math.sqrt((c1.x - c2.x) * (c1.x - c2.x) + (c1.y - c2.y) * (c1.y - c2.y));
        return distance <= r1 + r2;
    }
}
