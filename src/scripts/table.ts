import { Cercle, Dimension, Point, Rectangle } from "./models";

export class Table {
    static EPAISSEUR_BORDURE = 12.7;
    static EPAISSEUR_COUSSIN = 5.1;
    static dimensions: Dimension[] = [
        {
            id: 7,
            table: { h: 137, w: 236 },
            surface: { h: 99, w: 198 },
        },
        {
            id: 8,
            table: { h: 150, w: 262 },
            surface: { h: 112, w: 224 },
        },
        {
            id: 8.5,
            table: { h: 155, w: 272 },
            surface: { h: 117, w: 234 },
        },
        {
            id: 9,
            table: { h: 163, w: 290 },
            surface: { h: 127, w: 254 },
        },
    ];
    public surface: Rectangle;
    public coins: Cercle[];
    private origine: Point;
    private ctx: CanvasRenderingContext2D;
    private multiplicateur: number;
    private dimension: Dimension;

    constructor(ctx: CanvasRenderingContext2D, origine: Point, multiplicateur: number, dimension: Dimension) {
        this.ctx = ctx;
        this.origine = origine;
        this.multiplicateur = multiplicateur;
        this.dimension = dimension;
        this.surface = {
            x: origine.x + Table.EPAISSEUR_BORDURE * multiplicateur,
            y: origine.y + Table.EPAISSEUR_BORDURE * multiplicateur,
            w: (dimension.table.w - Table.EPAISSEUR_BORDURE * 2) * multiplicateur,
            h: (dimension.table.h - Table.EPAISSEUR_BORDURE * 2) * multiplicateur,
        };
        const rayon_trou = Table.EPAISSEUR_COUSSIN * this.multiplicateur;
        this.coins = [
            // Haut gauche
            { x: this.surface.x, y: this.surface.y, rayon: rayon_trou },
            // Haut milieu
            { x: this.surface.x + this.surface.w / 2, y: this.surface.y, rayon: rayon_trou },
            // Haut droite
            { x: this.surface.x + this.surface.w, y: this.surface.y, rayon: rayon_trou },
            // Bas gauche
            { x: this.surface.x, y: this.surface.y + this.surface.h, rayon: rayon_trou },
            // Bas milieu
            { x: this.surface.x + this.surface.w / 2, y: this.surface.y + this.surface.h, rayon: rayon_trou },
            // Bas droite
            { x: this.surface.x + this.surface.w, y: this.surface.y + this.surface.h, rayon: rayon_trou },
        ];
    }

    draw(ctx: CanvasRenderingContext2D): void {
        ctx.strokeStyle = "black";
        ctx.fillStyle = "white";
        ctx.fillRect(
            this.origine.x,
            this.origine.y,
            this.dimension.table.w * this.multiplicateur,
            this.dimension.table.h * this.multiplicateur
        );
        ctx.strokeRect(
            this.origine.x,
            this.origine.y,
            this.dimension.table.w * this.multiplicateur,
            this.dimension.table.h * this.multiplicateur
        );
        ctx.fillRect(this.surface.x, this.surface.y, this.surface.w, this.surface.h);
        ctx.strokeRect(this.surface.x, this.surface.y, this.surface.w, this.surface.h);

        // Coins
        this.coins.forEach((c) => {
            ctx.beginPath();
            ctx.arc(c.x, c.y, c.rayon, 0, 2 * Math.PI);
            ctx.fill();
            ctx.stroke();
            ctx.closePath();
        });
    }
}
