"use strict";

import { cloneDeep } from "lodash";
import "../styles/app.scss";
import { Boule } from "./boule";
import { BouleIteratif } from "./bouleIteratif";
import { Etat, Joueur } from "./etat";
import { IA } from "./ia";
import { IA2 } from "./ia2";
import { IA3 } from "./ia3";
import { Point, Rectangle, Vecteur } from "./models";
import { Prevision } from "./prevision";
import { Table } from "./table";

export let MULTIPLICATEUR = 4;
// restitution_collision_bord de la vitesse en heurtant les bords (%)
export const restitution_collision_bord = 0.6;
export const origine: Point = { x: 300, y: 100 };
export let surfaceJeu: Rectangle;
export const dimensions = Table.dimensions[1];
export const imgJoueur = new Image();
imgJoueur.src = "./img/joueur.svg";
export const imgOrdinateur = new Image();
imgOrdinateur.src = "./img/ordinateur.svg";
export let joueurHumain: Joueur | null = null;
export let difficulte: number | null = null;
export let joueurOrdinateur: Joueur | null = null;

let ctx: CanvasRenderingContext2D;
let table: Table;
let etats: Etat[] = [];
let canvasBillard: HTMLCanvasElement;
let positionCanvas: Point;
let puissance = 250;
let xSouris = 0;
let ySouris = 0;
let etatPrecedent: Etat | null = null;

const init = () => {
    const width = window.innerWidth;
    const height = window.innerHeight;
    const canvas: HTMLCanvasElement | null = <HTMLCanvasElement | null>document.getElementById("billard");
    canvas?.setAttribute("width", `${width}`);
    canvas?.setAttribute("height", `${height}`);
    MULTIPLICATEUR = Math.min(height / (dimensions.table.h * 1.5), width / (dimensions.table.w * 1.5));
    origine.x = (width - dimensions.table.w * MULTIPLICATEUR) / 2;
    origine.y = (height - dimensions.table.h * MULTIPLICATEUR) / 2;

    if (joueurHumain === null) {
        joueurHumain = confirm("Vous allez jouer Jaune. Voulez-vous jouer rouge ?") ? "Rouge" : "Jaune";
        joueurOrdinateur = joueurHumain === "Jaune" ? "Rouge" : "Jaune";
    }

    if (difficulte === null) {
        const difficulteStr = prompt(
            `Choix de la difficulté.
        1. Facile & rapide
        2. Moyen
        3. Dur & long`,
            "3"
        );
        if (difficulteStr) {
            difficulte = parseInt(difficulteStr);
        }
    }

    alert(`
Fautes :
    - Au premier tour le joueur change forcément
    - La bille noire est empochée avant que le joueur ait rentré toutes les billes de sa série.
    - Si le joueur ne touche pas de bille.
    - Si on ne touche pas une bille de son groupe en premier.
    - Si on touche la noire en premier contact.

Coup légal :
    - À chaque coup joué, le joueur doit frapper une bille de son groupe en 1er et soit :
        - Empocher une bille
        - Faire en sorte que la blanche ou une bille de son groupe touche la bande après le 1er contact

La bille blanche ne peut pas être empochée.`);

    if (canvas?.getContext) {
        canvasBillard = canvas;
        positionCanvas = getPositionCanvas(canvasBillard);
        // On récupère le contexte 2d du canvas
        ctx = <CanvasRenderingContext2D>canvas?.getContext("2d");
        if (difficulte === null) {
            init();
        } else {
            initialiserBillard();
        }
    } else {
        alert("Canvas non supporté");
    }
};

const initialiserBillard = (): void => {
    table = new Table(ctx, origine, MULTIPLICATEUR, dimensions);
    surfaceJeu = table.surface;
    const boules: BouleIteratif[] = [];
    for (let i = 1; i <= 16; i++) {
        let x = surfaceJeu.x + surfaceJeu.w / 2;
        let y = surfaceJeu.y + surfaceJeu.h / 2;
        const rayonBoulePx = Boule.RAYON_BOULE * MULTIPLICATEUR;
        const xRangee1 = surfaceJeu.x + 3 * (surfaceJeu.w / 4);
        const y1 = y;
        const xRangee2 = xRangee1 + Math.sqrt(3 * rayonBoulePx * rayonBoulePx);
        const xRangee3 = xRangee2 + Math.sqrt(3 * rayonBoulePx * rayonBoulePx);
        const xRangee4 = xRangee3 + Math.sqrt(3 * rayonBoulePx * rayonBoulePx);
        const xRangee5 = xRangee4 + Math.sqrt(3 * rayonBoulePx * rayonBoulePx);

        switch (i) {
            case 9:
                x = xRangee1;
                break;
            case 7:
                x = xRangee2;
                y = y1 - rayonBoulePx;
                break;
            case 12:
                x = xRangee2;
                y = y1 + rayonBoulePx;
                break;
            case 15:
                x = xRangee3;
                y = y1 - 2 * rayonBoulePx;
                break;
            case 8:
                x = xRangee3;
                y = y1;
                break;
            case 1:
                x = xRangee3;
                y = y1 + 2 * rayonBoulePx;
                break;
            case 6:
                x = xRangee4;
                y = y1 - 3 * rayonBoulePx;
                break;
            case 10:
                x = xRangee4;
                y = y1 - rayonBoulePx;
                break;
            case 3:
                x = xRangee4;
                y = y1 + rayonBoulePx;
                break;
            case 14:
                x = xRangee4;
                y = y1 + 3 * rayonBoulePx;
                break;
            case 11:
                x = xRangee5;
                y = y1 - 4 * rayonBoulePx;
                break;
            case 2:
                x = xRangee5;
                y = y1 - 2 * rayonBoulePx;
                break;
            case 13:
                x = xRangee5;
                y = y1;
                break;
            case 4:
                x = xRangee5;
                y = y1 + 2 * rayonBoulePx;
                break;
            case 5:
                x = xRangee5;
                y = y1 + 4 * rayonBoulePx;
                break;
            case 16:
                x = surfaceJeu.x + surfaceJeu.w / 4 - surfaceJeu.w / 20;
                break;
        }
        boules.push(new BouleIteratif(x, y, 0, 0, i));
    }
    const etatInitial = new Etat(0, boules, <Joueur>joueurHumain, 0, undefined);
    etatPrecedent = etatInitial;
    table.draw(ctx);
    etatInitial.draw(ctx);
    etats = [etatInitial];
    dessinEtats(etats, 0, false, true);
    canvasBillard.addEventListener("mousemove", mouvementSourisCanvas, false);
    canvasBillard.addEventListener("click", lancerCalcul, false);
    canvasBillard.addEventListener("wheel", scrollSourisCanvas, false);
    ctx.lineWidth = 3;
};

const calculEtats = (etatsPrecedents: Etat[]): Etat[] => {
    const prochainEtat = etatsPrecedents[etatsPrecedents.length - 1].getProchainEtat(surfaceJeu, restitution_collision_bord, table.coins);
    if (prochainEtat !== null) {
        return calculEtats([...etatsPrecedents, prochainEtat]);
    }
    return etatsPrecedents;
};

const calculEtatsNonDeep = (etatsPrecedents: Etat[]): Etat[] => {
    const prochainEtat = etatsPrecedents[etatsPrecedents.length - 1].getProchainEtatNonDeep(
        surfaceJeu,
        restitution_collision_bord,
        table.coins
    );
    if (prochainEtat !== null) {
        return calculEtatsNonDeep([...etatsPrecedents, prochainEtat]);
    }
    return etatsPrecedents;
};

const dessinEtats = (etats: Etat[], index: number, appelFinDessin: boolean, clear = true): void => {
    const etatActuel = etats[index];
    if (etatActuel) {
        if (clear) {
            ctx.clearRect(0, 0, canvasBillard.width, canvasBillard.height);
            table.draw(ctx);
        }
        etatActuel.draw(ctx);
        setTimeout(() => dessinEtats(etats, index + 1, appelFinDessin, clear), 16);
    } else {
        if (appelFinDessin) finDessin();
    }
};

const finDessin = (): void => {
    const etatFinal = etats[etats.length - 1];
    const isPartieFinie = etatFinal.isPartieFinie();
    const score = etatFinal.getScore();
    const scorePrecedent = etatPrecedent ? etatPrecedent.getScore() : { rouge: 0, jaune: 0 };
    const aFaute = etatFinal.aFaute(scorePrecedent);
    if (isPartieFinie) {
        let jauneVainqueur = etatFinal.joueur == "Jaune" && score.jaune === 7;
        jauneVainqueur = aFaute ? !jauneVainqueur : jauneVainqueur;
        const vainqueur: Joueur = jauneVainqueur ? "Jaune" : "Rouge";
        alert(`${vainqueur} gagne`);
        initialiserBillard();
    } else {
        if (aFaute) {
            etatFinal.changementJoueur();
        }
        if (etatFinal.joueur === joueurOrdinateur) {
            tourIA();
        } else {
            canvasBillard.addEventListener("mousemove", mouvementSourisCanvas, false);
            canvasBillard.addEventListener("click", lancerCalcul, false);
            canvasBillard.addEventListener("wheel", scrollSourisCanvas, false);
        }
    }
};

const tourIA = (): void => {
    const dernierEtat = etats[etats.length - 1];
    let tourAJouer: Vecteur = { vx: 0, vy: 0 };
    switch (difficulte) {
        case 1:
            tourAJouer = IA.calculTour(dernierEtat, table.surface, restitution_collision_bord, table.coins);
            break;
        case 2:
            tourAJouer = IA2.calculTour(dernierEtat.joueur, dernierEtat, table.surface, restitution_collision_bord, table.coins);
            break;
        case 3:
            tourAJouer = IA3.calculTour(dernierEtat.joueur, dernierEtat, table.surface, restitution_collision_bord, table.coins);
            break;
    }
    dernierEtat.getBouleBlanche().vx = tourAJouer.vx;
    dernierEtat.getBouleBlanche().vy = tourAJouer.vy;
    lancerCalcul();
};

const dessinVisee = (): void => {
    const dernierEtat = etats[etats.length - 1];
    const vx = xSouris - dernierEtat.getBouleBlanche().x;
    const vy = ySouris - dernierEtat.getBouleBlanche().y;
    const longueurVecteur = Math.sqrt(vx * vx + vy * vy);
    dernierEtat.getBouleBlanche().vx = (vx / longueurVecteur) * puissance * MULTIPLICATEUR;
    dernierEtat.getBouleBlanche().vy = (vy / longueurVecteur) * puissance * MULTIPLICATEUR;
    const previsions = Prevision.calculArriveeVecteur(surfaceJeu, dernierEtat.getBouleBlanche(), dernierEtat.getBoules());
    ctx.clearRect(0, 0, canvasBillard.width, canvasBillard.height);
    table.draw(ctx);
    ctx.strokeStyle = "black";
    dernierEtat.draw(ctx);
    for (let index = 0; index < previsions.length; index++) {
        const pre = previsions[index];
        ctx.lineWidth = 1;
        ctx.beginPath();
        ctx.moveTo(pre.depart.x, pre.depart.y);
        ctx.lineTo(pre.arrivee.x, pre.arrivee.y);
        ctx.stroke();
        if (pre.dessinBoule) {
            ctx.beginPath();
            ctx.arc(pre.arrivee.x, pre.arrivee.y, BouleIteratif.RAYON_BOULE * MULTIPLICATEUR, 0, 2 * Math.PI);
            ctx.stroke();
            ctx.closePath();
        }
    }
};

const mouvementSourisCanvas = (el: MouseEvent): void => {
    xSouris = el.clientX - positionCanvas.x;
    ySouris = el.clientY - positionCanvas.y;
    dessinVisee();
};

const scrollSourisCanvas = (el: WheelEvent): void => {
    if (el.deltaY < 0 && puissance * MULTIPLICATEUR < 1000) {
        puissance += 10 / MULTIPLICATEUR;
    } else if (el.deltaY > 0 && puissance * MULTIPLICATEUR > 0) {
        puissance -= 10 / MULTIPLICATEUR;
    }
    puissance = Math.max(0, puissance);
    dessinVisee();
};

function getPositionCanvas(el: any, xPosition = 0, yPosition = 0): Point {
    return el
        ? getPositionCanvas(
              el.offsetParent,
              xPosition + el.offsetLeft - el.scrollLeft + el.clientLeft,
              yPosition + el.offsetTop - el.scrollTop + el.clientTop
          )
        : { x: xPosition, y: yPosition };
}

function lancerCalcul(): void {
    canvasBillard.removeEventListener("mousemove", mouvementSourisCanvas, false);
    canvasBillard.removeEventListener("click", lancerCalcul, false);
    canvasBillard.removeEventListener("wheel", scrollSourisCanvas, false);
    puissance = 50;
    ctx.lineWidth = 1;
    const dernierEtat = etats[etats.length - 1];
    etats = [dernierEtat];
    etatPrecedent = cloneDeep(dernierEtat);
    dernierEtat.tourSuivant();

    const indexEtatPreCalcul = etats.length - 1;
    etats = calculEtats(etats);
    dessinEtats(etats, indexEtatPreCalcul, true, true);
}

// Lancement de la fonction d'initialisation au chargement de la page
window.onload = init;
window.onresize = init;
