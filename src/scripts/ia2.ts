import { cloneDeep } from "lodash";
import { Etat, Joueur, Score } from "./etat";
import { Rectangle, Cercle, Vecteur } from "./models";

export class IA2 {
    static calculTour(joueur: Joueur, etat: Etat, surface: Rectangle, restitution_collision_bord: number, coins: Cercle[]): Vecteur {
        const copieEtat = cloneDeep(etat);
        copieEtat.tourSuivant();
        const t1 = performance.now();
        const boules = etat.getBoules().filter((b) => b.numero !== 16);
        const bouleBlanche = etat.getBouleBlanche();
        if (boules.length === 0) {
            boules.push(...boules.filter((b) => b.numero === 8));
        }
        const vecteurs = boules.map((b) => {
            const vx = b.x - bouleBlanche.x;
            const vy = b.y - bouleBlanche.y;
            const longueurVecteur = Math.sqrt(vx * vx + vy * vy);
            return { num: b.numero, vx: (vx / longueurVecteur) * 1000, vy: (vy / longueurVecteur) * 1000 };
        });
        const etatsFinaux = vecteurs.map((v) => {
            const nouvelEtat = cloneDeep(copieEtat);
            nouvelEtat.getBouleBlanche().vx = v.vx;
            nouvelEtat.getBouleBlanche().vy = v.vy;
            const etatsFinaux = IA2.calculEtats([nouvelEtat], surface, restitution_collision_bord, coins);
            const etatFinal = etatsFinaux[etatsFinaux.length - 1];
            const scoreEtatFinal = IA2.evaluerEtat(joueur, etatFinal, etat.getScore(), v);
            return { v, etatFinal, scoreEtatFinal };
        });
        // console.log(etatsFinaux);
        etatsFinaux.sort((a, b) => b.scoreEtatFinal - a.scoreEtatFinal);
        const t2 = performance.now();
        // console.log(`Total tour IA : ${t2 - t1}`);
        return etatsFinaux[0].v;
    }

    static evaluerEtat(joueur: Joueur, etat: Etat, scorePrecedent: Score, v: { num: number; vx: number; vy: number }): number {
        const aFaute = etat.aFaute(scorePrecedent);
        const nouveauScore = etat.getScore();
        const partieFinie = etat.isPartieFinie();
        const joueurVainqueur = etat.joueur == joueur && nouveauScore.rouge === 8;
        const poidsFaute = -1;
        const poidsPointRouge = joueur === "Rouge" ? 2 : -2;
        const poidsPointJaune = joueur === "Jaune" ? 2 : -2;
        const poidsVictoire = 1000;
        const poidsDefaite = -1000;
        const totalFaute = aFaute ? poidsFaute : 0;
        const totalPointsRouge = (nouveauScore.rouge - scorePrecedent.rouge) * poidsPointRouge;
        const totalPointsJaune = (nouveauScore.jaune - scorePrecedent.jaune) * poidsPointJaune;
        const totalFin = partieFinie ? (joueurVainqueur ? poidsVictoire : poidsDefaite) : 0;
        return totalFaute + totalPointsRouge + totalPointsJaune + totalFin;
    }

    static calculEtats = (etatsPrecedents: Etat[], surface: Rectangle, restitution_collision_bord: number, coins: Cercle[]): Etat[] => {
        const prochainEtat = etatsPrecedents[etatsPrecedents.length - 1].getProchainEtatNonDeep(surface, restitution_collision_bord, coins);
        if (prochainEtat !== null) {
            return IA2.calculEtats([...etatsPrecedents, prochainEtat], surface, restitution_collision_bord, coins);
        }
        return etatsPrecedents;
    };
}
